# MOTOR.LIB

MOTOR.LIB Is a library used to control, design and develop human sensormitor experiments.

The library controls 2D and 3D robotic arm manipulands which are used in conjunction to
a virtual reality environment (2D: Monitor, 3D: Oculus) to simmulate novel dynamic environments
which human subjects have to interact with and perform skilled motor tasks.
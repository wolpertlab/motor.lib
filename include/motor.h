/******************************************************************************/
/*                                                                            */
/* MODULE  : motor.h                                                          */
/*                                                                            */
/* PURPOSE : General unclude file for C applications.                         */
/*                                                                            */
/* DATE    : 26/Apr/2000                                                      */
/*                                                                            */
/* CHANGES                                                                    */
/*                                                                            */
/* V2.0  JNI 26/Apr/2000 - Development taken over.                            */
/*                                                                            */
/* V2.1  JNI 29/Aug/2017 - Conditional compiles for full, 3BOT, mex versions. */
/*                                                                            */
/******************************************************************************/

#ifndef MOTOR_H
#define MOTOR_H

#define DB     fprintf(stderr,"Got to line %i in file %s\n",__LINE__,__FILE__);
#define DB_ESC fprintf(stderr,"Got to line %i in file %s (ESCape to stop).\n",__LINE__,__FILE__); if( getch() == ESC ) exit(0);

// Added this for Visual Studio 8
#define _CRT_SECURE_NO_DEPRECATE 1

#define _WIN32_WINNT 0x400

/***********************************/
/* Standard Visual C++ Includes... */
/***********************************/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <windows.h>
#include <sys\timeb.h>
#include <winioctl.h>
#include <sys\types.h>
#include <conio.h>
#include <io.h>
#include <fcntl.h>
#include <ctype.h>
#include <limits.h>
#include <direct.h>
#include <time.h>
#include <process.h>
#include <tchar.h>

/***********************************/
/* Nortern Digital OptoTrak...     */
/***********************************/
#if defined(MOTOR_FULL) || defined(MOTOR_3BOT)
    #include <ndhost.h>           // LIBRARY: oapi.lib
    #include <ndtypes.h>          // LIBRARY: oapi.lib
    #include <ndopto.h>           // LIBRARY: oapi.lib
#endif

/***********************************/
/* National Instruments DAQ...     */
/***********************************/
#if defined(MOTOR_XP)
    #include <nidaq.h>            // LIBRARY: nidaq32.lib
    #include <nidaqerr.h>
    #include <nidaqcns.h>
#endif

/***********************************/
/* SpaceWare input library...      */
/***********************************/
#if defined(MOTOR_FULL) || defined(MOTOR_3BOT)
    #include <si.h>               // LIBRARY: siapp{s,m}t.lib (s=single-thead; m=multi-thread)
#endif

/***********************************/
/* Cambridge Electronic Design...  */
/***********************************/
//#include <use1401.h>          // LIBRARY: use1432.lib

/***********************************/
/* GLEW...                         */
/***********************************/

#include <assert.h>
#if defined(MOTOR_3BOT)
    #include <glew.h>
#endif


/***********************************/
/* Oculus Rift HDM...              */
/***********************************/
#if defined(MOTOR_3BOT) 
    #undef main
    #include <OVR_CAPI.h>
    #include <OVR_CAPI_GL.h>
    extern "C" void ovrhmd_EnableHSWDisplaySDKRender( ovrSession hmd, ovrBool enabled );
#endif

/***********************************/
/* Open GL stuff...                */
/***********************************/
#if defined(MOTOR_FULL) || defined(MOTOR_3BOT)
    #include <glut.h>             // LIBRARY: glut32.lib
#endif

/***********************************/
/* Polhemus Liberty...             */
/***********************************/
#include <PDI.h>

/***********************************/
/* EyeLink-1000...                 */
/***********************************/
#include <gdi_expt.h>

#ifndef  PI
    #define PI 3.141592653589793
#endif

/***********************************/
/* In-House Includes...            */
/***********************************/
#include <typedefs.h>         // General types and definitions.
#include <version.h>          // LIBRARY: motor.lib MODULE: version.obj

#if defined(MOTOR_XP)
    #include <mem.h>              // LIBRARY: motor.lib MODULE: mem.obj
#endif

#include <matrix.h>           // LIBRARY: motor.lib MODULE: matrix.obj
#include <dataproc.h>         // LIBRARY: motor.lib MODULE: dataproc.obj
#include <timer.h>            // LIBRARY: motor.lib MODULE: timer.obj

#if defined(MOTOR_XP)
    #include <isa.h>              // LIBRARY: motor.lib MODULE: isa.obj
    #include <socket.h>           // LIBRARY: motor.lib MODULE: socket.obj
#endif

#include <spmx.h>             // LIBRARY: motor.lib MODULE: spmx.obj

#if defined(MOTOR_FULL) || defined(MOTOR_3BOT)
    #include <opto.h>             // LIBRARY: motor.lib MODULE: opto.obj,optobuff.obj,optofile.obj
    #include <general.h>          // LIBRARY: motor.lib MODULE: general.obj
#endif

#include <winfit.h>           // LIBRARY: motor.lib MODULE: winfit.obj
#include <kalmanfilter.h>     // LIBRARY: motor.lib MODULE: kalmanfilter.obj
#include <ekf.h>              // LIBRARY: motor.lib MODULE: ekf.obj
#include <sensoray.h>         // LIBRARY: motor.lib MODULE: sensoray.obj
#include <phantomisa.h>       // LIBRARY: motor.lib MODULE: phantomisa.obj

#include <controller.h>       // LIBRARY: motor.lib MODULE: controller.obj
#include <temptrak.h>         // LIBRARY: motor.lib MODULE: temptrak.obj
#include <asensor.h>          // LIBRARY: motor.lib MODULE: asensor.obj
#include <ramper.h>           // LIBRARY: motor.lib MODULE: ramper.obj
#include <robot-object.h>     // LIBRARY: motor.lib MODULE: robot-object.obj
#include <robot.h>            // LIBRARY: motor.lib MODULE: robot.obj

#if defined(MOTOR_XP)
    #include <atift.h>            // LIBRARY: motor.lib MODULE: atift.obj
#endif

//#include <ced.h>              // LIBRARY: motor.lib MODULE: ced.obj

#include <config.h>           // LIBRARY: motor.lib MODULE: config.obj

#if defined(MOTOR_FULL) || defined(MOTOR_3BOT)
    #include <metaconfig.h>       // LIBRARY: motor.lib MODULE: metaconfig.obj
    #include <matdat.h>           // LIBRARY: motor.lib MODULE: matdat.obj
    #include <datafile.h>         // LIBRARY: motor.lib MODULE: datafile.obj
    #include <graphics.h>         // LIBRARY: motor.lib MODULE: graphics.obj
    #include <graphics_image.h>   // LIBRARY: motor.lib MODULE: graphics_image.obj
#endif

#include <mmtimer.h>          // LIBRARY: motor.lib MODULE: mmtimer.obj
#include <looptask.h>         // LIBRARY: motor.lib MODULE: looptask.obj

#if defined(MOTOR_XP)
    #include <fob.h>              // LIBRARY: motor.lib MODULE: fob.obj
    #include <cyberglove.h>       // LIBRARY: motor.lib MODULE: cyberglove.obj
#endif

#include <str.h>              // LIBRARY: motor.lib MODULE: str.obj

#if defined(MOTOR_FULL) || defined(MOTOR_3BOT)
    #include <optorigid.h>        // LIBRARY: motor.lib MODULE: optorigid.obj
#endif

#if defined(MOTOR_XP)
    #include <goggles.h>          // LIBRARY: motor.lib MODULE: goggles.obj
    #include <trigger.h>          // LIBRARY: motor.lib MODULE: trigger.obj
#endif

#include <cmdarg.h>           // LIBRARY: motor.lib MODULE: cmdarg.obj

#if defined(MOTOR_XP)
    #include <com.h>              // LIBRARY: motor.lib MODULE: com.obj
#endif

#include <kb.h>               // LIBRARY: motor.lib MODULE: kb.obj
#include <atexit.h>           // LIBRARY: motor.lib MODULE: atexit.obj
#include <file.h>             // LIBRARY: motor.lib MODULE: file.obj

#if defined(MOTOR_XP)
    #include <stim.h>             // LIBRARY: motor.lib MODULE: stim.obj
    #include <snms.h>             // LIBRARY: motor.lib MODULE: snms.obj
    #include <ni-daq.h>           // LIBRARY: motor.lib MODULE: ni-daq.obj
#endif

#if defined(MOTOR_FULL) || defined(MOTOR_3BOT)
    #include <mouse.h>            // LIBRARY: motor.lib MODULE: mouse.obj
#endif

#include <liberty.h>          // LIBRARY: motor.lib MODULE: liberty.obj

#if defined(MOTOR_XP)
    #include <tracking.h>         // LIBRARY: motor.lib MODULE: tracking.obj
    #include <beeper.h>           // LIBRARY: motor.lib MODULE: beeper.obj
    #include <button.h>           // LIBRARY: motor.lib MODULE: button.obj
#endif

#if defined(MOTOR_FULL) || defined(MOTOR_3BOT)
    #include <waveplay.h>         // LIBRARY: motor.lib MODULE: waveplay.obj
    #include <devices.h>          // LIBRARY: motor.lib MODULE: devices.obj
    #include <permutelist.h>      // LIBRARY: motor.lib MODULE: permutelist.obj
#endif

#include <pmove.h>            // LIBRARY: motor.lib MODULE: pmove.obj
#include <EyeLink1000.h>      // LIBRARY: motor.lib MODULE: EyeLink1000.obj

#if defined(MOTOR_FULL) || defined(MOTOR_3BOT)
    #include <EyeT.h>             // LIBRARY: motor.lib MODULE: EyeT.obj
    #include <oculus.h>           // LIBRARY: motor.lib MODULE: oculus.obj
#endif

/***********************************/
/* GLM library (Graphical OBJs)... */
/***********************************/
//#include <glm.h>

/******************************************************************************/

#endif  MOTOR_H


/******************************************************************************/
/*                                                                            */
/* MODULE  : GRAPHICS_Image.h                                                 */
/*                                                                            */
/* PURPOSE : GRAPHICS sub-module to display BMP images.                       */
/*                                                                            */
/* DATE    : 22/Oct/2016                                                      */
/*                                                                            */
/* CHANGES                                                                    */
/*                                                                            */
/* V1.0  JNI 22/Oct/2016 - Initial development from local application code.   */
/*                                                                            */
/******************************************************************************/

#include "glext2.h"

/******************************************************************************/

class AUX_RGBImageRec2 
{
    void convertBGRtoRGB( void );

public:
    byte *data;
    DWORD sizeX;
    DWORD sizeY;
    bool NoErrors;
    AUX_RGBImageRec2( void ): NoErrors(false), data(NULL) { };
    AUX_RGBImageRec2( const char *FileName );
   ~AUX_RGBImageRec2( void );
    bool loadFile( const char *FileName );

    friend AUX_RGBImageRec2 *auxDIBImageLoad2( const char *FileName );
};

/******************************************************************************/

class GRAPHICS_Image
{
public:

    BOOL OpenFlag;
    BOOL LoadedFlag;

    STRING FileName;

    int TextureIndex;
    GLuint TextureID;

    AUX_RGBImageRec2 *TextureImage;

    int Xpixels;
    int Ypixels;

    double AspectRatio;

    double Xsize;
    double Ysize;

    double Xhalf;
    double Yhalf;

    TIMER_Interval *DrawLatency;

    GRAPHICS_Image( void );
   ~GRAPHICS_Image( void );

    BOOL Open( char *filename, double xsize, double ysize );
    BOOL Open( char *filename, double xsize );
    BOOL Open( void );
    void Close( void );

    BOOL Load( char *filename, double xsize, double ysize );
    BOOL Load( char *filename, double xsize );

    void Draw( matrix *posn );
    void Draw( void );

    void ImagePixels( int &xpixels, int &ypixels );
    void ImageSize( int &xsize, int &ysize );
};

/******************************************************************************/

#define GRAPHICS_IMAGE_MAX   8

void GRAPHICS_ImageStart( void );
void GRAPHICS_ImageStop( void );

/******************************************************************************/


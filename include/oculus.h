/******************************************************************************/
/*                                                                            */
/* MODULE  : Oculus.h                                                         */
/*                                                                            */
/* PURPOSE : Oculus Rift HMD module.                                          */
/*                                                                            */
/* DATE    : 09/Jan/2015                                                      */
/*                                                                            */
/* CHANGES                                                                    */
/*                                                                            */
/* V1.0  JNI 09/Jan/2015 - Initial development.                               */
/*                                                                            */
/* V1.1  JNI 10/Sep/2015 - Re-visit to solve rotation / workspace issue.      */
/*                                                                            */
/******************************************************************************/

#define OCULUS_DEBUG() if( !OCULUS_API_start(printf,printf,printf) ) { printf("Cannot start OCULUS API.\n"); exit(0); }

/******************************************************************************/

extern  PRINTF  OCULUS_PRN_messgf;      // General messages printf function.
extern  PRINTF  OCULUS_PRN_errorf;      // Error messages printf function.
extern  PRINTF  OCULUS_PRN_debugf;      // Debug information printf function.

#define OCULUS_MinX OCULUS_DisplaySize[GRAPHICS_X][GRAPHICS_MIN]
#define OCULUS_MaxX OCULUS_DisplaySize[GRAPHICS_X][GRAPHICS_MAX]
#define OCULUS_MinY OCULUS_DisplaySize[GRAPHICS_Y][GRAPHICS_MIN]
#define OCULUS_MaxY OCULUS_DisplaySize[GRAPHICS_Y][GRAPHICS_MAX]
#define OCULUS_MinZ OCULUS_DisplaySize[GRAPHICS_Z][GRAPHICS_MIN]
#define OCULUS_MaxZ OCULUS_DisplaySize[GRAPHICS_Z][GRAPHICS_MAX]

#define OCULUS_CONFIG "OCULUS.CFG"

/******************************************************************************/

int  OCULUS_messgf( const char *mask, ... );
int  OCULUS_errorf( const char *mask, ... );
int  OCULUS_debugf( const char *mask, ... );

BOOL OCULUS_API_start( PRINTF messgf, PRINTF errorf, PRINTF debugf );
void OCULUS_API_stop( void );

BOOL OCULUS_Config( char *file );

BOOL OCULUS_Check( void );
BOOL OCULUS_Open( void );
void OCULUS_Close( void );
BOOL OCULUS_Opened( void );
BOOL OCULUS_Start( void );
void OCULUS_Stop( void );
void OCULUS_RenderTarget( int width, int height );

void OCULUS_GraphicsDisplay( void (*draw)( void ) );
void OCULUS_GraphicsDisplay( void );

void OCULUS_QuaternionToRotationMatrix( float *quat, float *mat );
UINT OCULUS_NextPower2( UINT x );

void OCULUS_GridBuild( float xmin, float xmax, float ymin, float ymax, float zpos, int grid, int ID );
void OCULUS_GridDraw( void );
void OCULUS_GridBuild( void );

void OCULUS_DisplayPosition( int x, int y );
void OCULUS_DisplayOnHMD( void );
void OCULUS_DisplayOnDeskTop( void );
void OCULUS_DisplayToggle( void );

void OCULUS_TimingResults( void );

BOOL OCULUS_GlutKeyboard( BYTE key, int x, int y );

/******************************************************************************/

/*
// typedef Rect<int> Recti;
#include "OVR_CAPI_GL.h"
// using namespace OVR;

struct DepthBuffer
{
    GLuint        texId;

    DepthBuffer(ovrSizei size, int sampleCount)
    {

        glGenTextures(1, &texId);
        glBindTexture(GL_TEXTURE_2D, texId);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        GLenum internalFormat = GL_DEPTH_COMPONENT24;
        GLenum type = GL_UNSIGNED_INT;

        glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, size.w, size.h, 0, GL_DEPTH_COMPONENT, type, NULL);
    }
    ~DepthBuffer()
    {
        if (texId)
        {
            glDeleteTextures(1, &texId);
            texId = 0;
        }
    }
};
struct TextureBuffer
{
    ovrHmd              hmd;
    ovrSwapTextureSet*  TextureSet;
    GLuint              texId;
    GLuint              fboId;
    ovrSizei               texSize;

    TextureBuffer(ovrHmd hmd, bool rendertarget, bool displayableOnHmd, ovrSizei size, int mipLevels, unsigned char * data, int sampleCount) :
        hmd(hmd),
        TextureSet(nullptr),
        texId(0),
        fboId(0)

    {

        texSize = size;

        if (displayableOnHmd)
        {
            // This texture isn't necessarily going to be a rendertarget, but it usually is.


            ovrResult result = ovr_CreateSwapTextureSetGL(hmd, GL_SRGB8_ALPHA8, size.w, size.h, &TextureSet);

            if(OVR_SUCCESS(result))
            {
                for (int i = 0; i < TextureSet->TextureCount; ++i)
                {
                    ovrGLTexture* tex = (ovrGLTexture*)&TextureSet->Textures[i];
                    glBindTexture(GL_TEXTURE_2D, tex->OGL.TexId);

                    if (rendertarget)
                    {
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
                    }
                    else
                    {
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
                    }
                }
            }
        }
        else
        {
            glGenTextures(1, &texId);
            glBindTexture(GL_TEXTURE_2D, texId);

            if (rendertarget)
            {
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            }
            else
            {
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            }

            glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB8_ALPHA8, texSize.w, texSize.h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        }

        if (mipLevels > 1)
        {
            glGenerateMipmap(GL_TEXTURE_2D);
        }

        glGenFramebuffers(1, &fboId);
    }

    ~TextureBuffer()
    {
        if (TextureSet)
        {
            ovr_DestroySwapTextureSet(hmd, TextureSet);
            TextureSet = nullptr;
        }
        if (texId)
        {
            glDeleteTextures(1, &texId);
            texId = 0;
        }
        if (fboId)
        {
            glDeleteFramebuffers(1, &fboId);
            fboId = 0;
        }
    }

    ovrSizei GetSize() const
    {
        return texSize;
    }

    void SetAndClearRenderSurface(DepthBuffer* dbuffer)
    {
        auto tex = reinterpret_cast<ovrGLTexture*>(&TextureSet->Textures[TextureSet->CurrentIndex]);

        glBindFramebuffer(GL_FRAMEBUFFER, fboId);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex->OGL.TexId, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, dbuffer->texId, 0);

        glViewport(0, 0, texSize.w, texSize.h);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_FRAMEBUFFER_SRGB);
    }

    void UnsetRenderSurface()
    {
        glBindFramebuffer(GL_FRAMEBUFFER, fboId);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0, 0);
    }
};

//--------------------------------------------------------------------------------------
*/

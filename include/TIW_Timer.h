#include <strsafe.h>
#include <motor.h>

#define THREAD_NUM 2
#define BUF_SIZE 255
// TODO: Turn the high precision windows multimedia player on as per the paper



typedef struct ThreadData {
    int a_on;
    int duration;
    int frequency;
    void (*callback)(void);
    double sigma_previous;
    double sigma_current;
} THRD_DATA;



void TIWTimer( int duration, int frequency,  void (*callback)(void)  );
DWORD WINAPI CheckTime( void * lpParam);
void ErrorHandler(char * lpszFunction);
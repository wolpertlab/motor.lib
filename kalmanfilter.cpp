/******************************************************************************/
/*                                                                            */
/* MODULE  : KalmanFilter.cpp                                                 */
/*                                                                            */
/* PURPOSE : Kalman Filter class.                                             */
/*                                                                            */
/* DATE    : 24/Apr/2007                                                      */
/*                                                                            */
/* CHANGES                                                                    */
/*                                                                            */
/* V1.0  JNI 24/Apr/2007 - Initial development.                               */
/*                                                                            */
/******************************************************************************/

#include <motor.h>

/******************************************************************************/

int KALMANFILTER_Index=0;

/******************************************************************************/

KALMANFILTER::KALMANFILTER( char *name )
{
    Init(name);
}

/******************************************************************************/

KALMANFILTER::KALMANFILTER( void )
{
    Init(NULL);
}

/******************************************************************************/

KALMANFILTER::~KALMANFILTER( )
{
    if( timer != NULL )
    {
        delete timer;
        timer = NULL;
    }

    if( freq != NULL )
    {
        delete freq;
        freq = NULL;
    }

    DataFree();
}

/******************************************************************************/

void KALMANFILTER::Init( void )
{
int i;

    memset(ObjectName,0,STRLEN);
    OpenFlag = FALSE;
    FirstFlag = FALSE;

    timer = NULL;
    freq = NULL;
    tlast = 0.0;

    for( i=0; (i < KALMANFILTER_DATA); i++ )
    {
        data[i] = NULL;
    }
}

/******************************************************************************/

void KALMANFILTER::Init( char *name )
{
    Init();

    if( name == NULL )
    {
        strncpy(ObjectName,STR_stringf("KALMAN%d",KALMANFILTER_Index),STRLEN);
    }
    else
    {
        strncpy(ObjectName,name,STRLEN);
    }

    timer = new TIMER(ObjectName);
    freq = new TIMER_Frequency(ObjectName);

    H.dim(1,2);
    Q.dim(2,2);
    R.dim(1,1);
    x.dim(2,1);
    P.dim(2,2);
    A.dim(2,2);

    A(1,1) = 1.0;
    A(1,2) = 0.0;
    A(2,2) = 1.0;
    A(2,1) = 0.0;

    H(1,1) = 1.0;
    H(1,2) = 0.0;

    KALMANFILTER_Index++;
}

/******************************************************************************/

void KALMANFILTER::Calculate( double xraw, double dtime, double &xfilt, double &dxfilt )
{   
    if( !OpenFlag )
    {
        return;
    }

    freq->Loop();
  
    A(1,2) = dtime;

    Q(1,1) = pow(dtime,4.0) / 4.0;
    Q(1,2) = pow(dtime,3.0) / 2.0;
    Q(2,2) = pow(dtime,2.0);
    Q(2,1) = Q(1,2);
    Q *= (w*w);

    if( FirstFlag )
    {
        x(1,1) = xraw;
        x(2,1) = 0.0;
        P = Q;

        FirstFlag = FALSE;
    }

    xm = A*x;
    Pm = (A*P*T(A))+Q;
    K = Pm*T(H)*inv((H*Pm*T(H))+R);
    x = xm+K*(xraw-H*xm);
    P = (I(2)-K*H)*Pm;

    xfilt = x(1,1);
    dxfilt = x(2,1); 

    if( data[0] != NULL ) data[0]->Data(dtime);
    if( data[1] != NULL ) data[1]->Data(xraw);
    if( data[2] != NULL ) data[2]->Data(xfilt);
    if( data[3] != NULL ) data[3]->Data(dxfilt);
    /*printf("Deleting A\n");
    delete &A;
    printf("Deleted A\n");*/
}

/******************************************************************************/

void KALMANFILTER::Calculate( double xraw, double &xfilt, double &dxfilt )
{
double t,dtime;

    if( !OpenFlag )
    {
        return;
    }

    t = timer->ElapsedSeconds();

    if( FirstFlag )
    {
        dtime = dt;
    }
    else
    {
        dtime = t - tlast;
    }

    tlast = t;

    Calculate(xraw,dtime,xfilt,dxfilt);
}

/******************************************************************************/

BOOL KALMANFILTER::DataSave( )
{
int i;
BOOL ok;

    if( !OpenFlag )
    {
        return(FALSE);
    }

    for( ok=TRUE,i=0; (i < KALMANFILTER_DATA); i++ )
    {
    	if( data[i] != NULL )
    	{
            if( !data[i]->Save() )
            {
                ok = FALSE;
            }
        }
    }

    return(ok);
}

/******************************************************************************/

void KALMANFILTER::DataFree( )
{
int i;

    for( i=0; (i < KALMANFILTER_DATA); i++ )
    {
    	if( data[i] != NULL )
    	{
            delete data[i];
            data[i] = NULL;
        }
    }
}

/******************************************************************************/

BOOL KALMANFILTER::Opened( void )
{
    return(OpenFlag);
}

/******************************************************************************/

void KALMANFILTER::Open( double kf_w, double kf_v, double kf_dt )
{
    Open(kf_w,kf_v,kf_dt,0);
}

/******************************************************************************/

void KALMANFILTER::Open( double kf_w, double kf_v, double kf_dt, int ditems )
{
int i;

    printf("%s: w=%.3lf v=%.3lf dt=%.3lf\n",ObjectName,kf_w,kf_v,kf_dt);

    // Don't open the object if parameters are null...
    if( (kf_w * kf_v * kf_dt) == 0.0 )
    {
        return;
    }

    if( ditems > 0 )
    {
        for( i=0; (i < KALMANFILTER_DATA); i++ )
        {
            data[i] = new DATAPROC(STR_stringf("%s-%d",ObjectName,i),ditems);
        }
    }

    w = kf_w;
    v = kf_v;
    dt = kf_dt;

    R(1,1) = v*v;

    FirstFlag = TRUE;
    OpenFlag = TRUE;
}

/******************************************************************************/

void KALMANFILTER::Close( void )
{
BOOL ok;

    OpenFlag = FALSE;

    ok = DataSave();
    DataFree();
}

/******************************************************************************/


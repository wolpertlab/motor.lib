/******************************************************************************/
/*                                                                            */ 
/* MODULE  : VERSION.cpp                                                      */ 
/*                                                                            */ 
/* PURPOSE : Version registry API for library modules.                        */ 
/*                                                                            */ 
/* DATE    : 16/May/2000                                                      */ 
/*                                                                            */ 
/* CHANGES                                                                    */ 
/*                                                                            */ 
/* V1.0  JNI 16/May/2000 - Initial version.                                   */ 
/*                                                                            */ 
/* V1.1  JNI 04/Feb/2016 - Tweaks include MOTOR.LIB initialization function.  */ 
/*                                                                            */ 
/* V1.2  JNI 29/Aug/2017 - Conditional compile for FULL versus MEX versions.  */ 
/*                                                                            */ 
/******************************************************************************/

#define MODULE_NAME     "VERSION"
#define MODULE_TEXT     "Version Registry API"
#define MODULE_DATE     "29/08/2017"
#define MODULE_VERSION  "1.2"

/******************************************************************************/

#include <motor.h>                               // Includes everything we need.

/******************************************************************************/

struct  VERSION_RegistryItem      VERSION_Item[VERSION_LIST];
BOOL    VERSION_API_started=FALSE;

/******************************************************************************/

PRINTF  VERSION_PRN_messgf=NULL;                 // General messages printf function.
PRINTF  VERSION_PRN_errorf=NULL;                 // Error messages printf function.
PRINTF  VERSION_PRN_debugf=NULL;                 // Debug information printf function.

/******************************************************************************/
/* API print functions for different message types...                         */
/******************************************************************************/

int    VERSION_messgf( const char *mask, ... )
{
va_list args;
static  char  buff[PRNTBUFF];

    va_start(args,mask);
    vsprintf(buff,mask,args);
    va_end(args);

    return(STR_printf(VERSION_PRN_messgf,buff));
}

/******************************************************************************/

int     VERSION_errorf( const char *mask, ... )
{
va_list args;
static  char  buff[PRNTBUFF];

    va_start(args,mask);
    vsprintf(buff,mask,args);
    va_end(args);

    return(STR_printf(VERSION_PRN_errorf,buff));
}

/******************************************************************************/

int     VERSION_debugf( const char *mask, ... )
{
va_list args;
static  char  buff[PRNTBUFF];

    va_start(args,mask);
    vsprintf(buff,mask,args);
    va_end(args);

    return(STR_printf(VERSION_PRN_debugf,buff));
}

/******************************************************************************/

int     VERSION_printf( short item, const char *mask, ... )
{
va_list args;
char    buff[PRNTBUFF];
int     plen=0;

    va_start(args,mask);
    vsprintf(buff,mask,args);
    va_end(args);

    if( VERSION_Item[item].prnf != NULL )
    {
        plen = (*VERSION_Item[item].prnf)(buff);
    }
    else
    {

        plen = VERSION_messgf(buff);
    }

    return(plen);
}

/******************************************************************************/

BOOL     VERSION_API_start( PRINTF messgf, PRINTF errorf, PRINTF debugf )
{
short    item;
BOOL     ok=FALSE;

#ifdef VERSION_HACK
    return(TRUE);
#endif

    if( VERSION_API_started )          // Start the API once...
    {
        return(TRUE);
    }

    VERSION_PRN_messgf = messgf;       // General API message print function.
    VERSION_PRN_errorf = errorf;       // API error message print function.
    VERSION_PRN_debugf = debugf;       // Debug information print function.

    for( item=0; (item < VERSION_LIST); item++ )
    {
        memset(&VERSION_Item[item],0,sizeof(struct VERSION_RegistryItem));
        VERSION_Item[item].used = FALSE;
    }

    ok = TRUE;

    if( ok )
    {
//      ATEXIT_API(VERSION_API_stop);       // Install stop function.
        VERSION_API_started = TRUE;         // Set started flag.

        MODULE_start(VERSION_PRN_messgf);   // Register module.
    }
    else
    {
        VERSION_errorf("VERSION_API_start(...) Failed.\n");
    }

    return(ok);
}

/******************************************************************************/

void    VERSION_API_stop( void )
{
short   item;

#ifdef VERSION_HACK
    return;
#endif

    if( !VERSION_API_started )         // API not started in the first place...
    {
        return;
    }

    MODULE_stop();
    VERSION_API_started = FALSE;
}

/******************************************************************************/

BOOL    VERSION_API_check( void )
{
BOOL    ok=TRUE;

#ifdef VERSION_HACK
    return(TRUE);
#endif

    if( !VERSION_API_started )         // API not started...
    {                                  // Start module automatically...
        ok = VERSION_API_start(MODULE_messgf(),MODULE_errorf(),MODULE_debugf());
        VERSION_debugf("VERSION_API_check() Start %s.\n",ok ? "OK" : "Failed");
    }

    return(ok);
}

/******************************************************************************/

short   VERSION_find( char *name )
{
short   item,find=VERSION_ITEM_ERROR;

    for( item=0; (item < VERSION_LIST); item++ )
    {
        if( name != NULL )        // Find specific module name...
        {
            if( strcmp(VERSION_Item[item].name,name) == 0 )
            {
                find = item;
                break;
             }
        }
        else                      // Find a free slot...
        {
            if( !VERSION_Item[item].used )
            {
                find = item;
                break;
            }
        }
    }

    return(find);
}

/******************************************************************************/

void    VERSION_start( char *name, char *text, char *date, char *vers, PRINTF prnf )
{
short   item;

#ifdef VERSION_HACK
    return;
#endif

    if( !VERSION_API_check() )              // Make sure API is started...
    {
        return;
    }

    if( (item=VERSION_find(name)) == VERSION_ITEM_ERROR )
    {                                  // Module not already in list...
        if( (item=VERSION_find(NULL)) != VERSION_ITEM_ERROR )
        {                              // Add module to registry list...
            VERSION_Item[item].used = TRUE;
            VERSION_Item[item].prnf = prnf;

            strncpy(VERSION_Item[item].name,name,STRLEN);
            strncpy(VERSION_Item[item].text,text,STRLEN);
            strncpy(VERSION_Item[item].date,date,STRLEN);
            strncpy(VERSION_Item[item].vers,vers,STRLEN);
        }
    }

    if( item == VERSION_ITEM_ERROR )         // List full or something not right...
    {
        return;
    }

//  Print module start message...
    VERSION_printf(item,"%s V%s Started.\n",VERSION_Item[item].text,VERSION_Item[item].vers);

    VERSION_Item[item].started = TRUE;      // Module is running... 
}

/******************************************************************************/

void    VERSION_stop( short item )
{
#ifdef VERSION_HACK
    return;
#endif

    if( !VERSION_API_check() )              // Make sure API is started...
    {
        return;
    }

    if( !VERSION_Item[item].used )          // Module registry item not used...
    {
        return;
    }

    if( !VERSION_Item[item].started )       // Module already stopped...
    {
        return;
    }

//  Print module stop messsage...
    VERSION_printf(item,"%s V%s Stopped.\n",VERSION_Item[item].text,VERSION_Item[item].vers);

    VERSION_Item[item].started = FALSE;     // Module is not running... 
}

/******************************************************************************/

void    VERSION_stop( char *name )
{
short   item;

#ifdef VERSION_HACK
    return;
#endif

    if( !VERSION_API_check() )              // Make sure API is started...
    {
        return;
    }

    if( (item=VERSION_find(name)) >= VERSION_LIST )
    {                                       // Module not in list...
        return;
    }

    VERSION_stop(item);                     // Stop module... 
}

/******************************************************************************/

void    VERSION_list( PRINTF prnf )
{
short   item;

    if( !VERSION_API_check() )              // Make sure API is started...
    {
        return;
    }

    for( item=0; (item < VERSION_LIST); item++ )
    {
        if( !VERSION_Item[item].used )
        {
            continue;
        }

      (*prnf)("%-12s V%s %s %s\n",
              VERSION_Item[item].name,
              VERSION_Item[item].vers,
              VERSION_Item[item].date,
              VERSION_Item[item].text,
              VERSION_Item[item].started ? "Started" : "Stopped");
    }
}

/******************************************************************************/

void VERSION_list( void )
{
    VERSION_list(printf);
}

/******************************************************************************/

short   VERSION_used( void )
{
short   item,used;

    if( !VERSION_API_check() )              // Make sure API is started...
    {
        return(VERSION_ITEM_ERROR);
    }

    for( used=0,item=0; (item < VERSION_LIST); item++ )
    {
        if( VERSION_Item[item].used )
        {
            used++;
        }
    }

    used++;

    return(used);
}

/******************************************************************************/

short   VERSION_started( void )
{
short   item,started;

    if( !VERSION_API_check() )              // Make sure API is started...
    {
        return(VERSION_ITEM_ERROR);
    }

    for( started=0,item=0; (item < VERSION_LIST); item++ )
    {
        if( VERSION_Item[item].used && VERSION_Item[item].started )
        {
            started++;
        }
    }

    return(started);
}

/******************************************************************************/

STRING  MODULE_PrintName[MODULE_PRINT] = { "ModuleMessg","ModuleError","ModuleDebug" };
STRING  MODULE_PrintList[MODULE_PRINT] = { "","","" };
BOOL    MODULE_PrintLoaded=FALSE;

/******************************************************************************/

void MODULE_PrintLoad( void )
{
int type;
DWORD size;

    for( type=MODULE_MESSG; (type <= MODULE_DEBUG); type++ )
    {
        size = GetEnvironmentVariable(MODULE_PrintName[type],MODULE_PrintList[type],STRLEN);
    }
}

/******************************************************************************/

BOOL MODULE_PrintFind( int type, char *module )
{
BOOL flag=FALSE;

    if( !MODULE_PrintLoaded )
    {
        MODULE_PrintLoad();
        MODULE_PrintLoaded = TRUE;
    }

    if( strstr(MODULE_PrintList[type],module) != NULL )
    {
        flag = TRUE;
    }

    return(flag);
}

/******************************************************************************/

int     MODULE_PrintError( const char *mask, ... )
{
va_list args;
char    buff[PRNTBUFF];
int     plen=0;
PRINTF  pfunc=printf;

    va_start(args,mask);
    vsprintf(buff,mask,args);
    va_end(args);

    plen = (*pfunc)(buff);
    
    return(plen);
}

/******************************************************************************/

int     MODULE_PrintMessg( const char *mask, ... )
{
va_list args;
char    buff[PRNTBUFF];
int     plen=0;
PRINTF  pfunc=printf;

    va_start(args,mask);
    vsprintf(buff,mask,args);
    va_end(args);

    plen = (*pfunc)(buff);
    
    return(plen);
}
/******************************************************************************/

int     MODULE_PrintDebug( const char *mask, ... )
{
va_list args;
char    buff[PRNTBUFF];
int     plen=0;
PRINTF  pfunc=printf;

    va_start(args,mask);
    vsprintf(buff,mask,args);
    va_end(args);

    plen = (*pfunc)(buff);
    
    return(plen);
}

/******************************************************************************/

PRINTF MODULE_printf( char *module, int type )
{
PRINTF pfunc=NULL;
BOOL flag;

    flag = MODULE_PrintFind(type,module);

    switch( type )
    {
        case MODULE_MESSG :
           if( flag )
           {
               pfunc = MODULE_PrintMessg;
           }
           break;

        case MODULE_ERROR :
           pfunc = MODULE_PrintError;
           break;

        case MODULE_DEBUG :
           if( flag )
           {
               pfunc = MODULE_PrintDebug;
           }
           break;
    }

    return(pfunc);
}

/******************************************************************************/

BOOL   MOTOR_InitFlag=FALSE;

STRING MOTOR_ProgramExeFile="";
STRING MOTOR_ProgramCppFile="";

/******************************************************************************/

BOOL MOTOR_Initialized( void )
{
BOOL ok=TRUE;

    if( !MOTOR_InitFlag )
    {
        printf("MOTOR.LIB not initialized.\n");
        ok = FALSE;
    }

    return(ok);
}

/******************************************************************************/

void MOTOR_Init( int argc, char *argv[], BYTE flag, double loopfreq )
{
BOOL ok=TRUE;

    if( (argc == 0) || (argv == NULL ) )
    {
        printf("MOTOR_Init(flag=0x%02X,loopfreq=%.0fHz)\n",flag,loopfreq);
    }
    else
    {
        printf("MOTOR_Init(argc=%d,argv[0]=%s,flag=0x%02X,loopfreq=%.0fHz)\n",argc,argv[0],flag,loopfreq);
    }

    if( MOTOR_InitFlag )
    {
        return;
    }

    // Seed random number generator?
    if( (flag & MOTOR_FLAG_NOSEED) == 0 )
    {
        randomize();
    }

    if( loopfreq != 0.0 )
    {
        LOOPTASK_frequency(loopfreq);
    }

    // Patch High-Performance-Counter bug using LoopTask?
    if( (flag & MOTOR_FLAG_NOTIMERPATCH) == 0 )
    {
        LOOPTASK_TimerSkipPatch();
    }

    // Check for EXE/CPP files using argv[0] name-of-executable?
    if( (flag & MOTOR_FLAG_NOARGZERO) == 0 )
    {
        strncpy(MOTOR_ProgramExeFile,STR_stringf("%s.exe",argv[0]),STRLEN);
        strncpy(MOTOR_ProgramCppFile,STR_stringf("%s.cpp",argv[0]),STRLEN);

        if( !FILE_Exist(MOTOR_ProgramExeFile) )
        {
            printf("Cannot find EXE file: %s\n",MOTOR_ProgramExeFile);
            ok = FALSE;
        }

        if( !FILE_Exist(MOTOR_ProgramCppFile) )
        {
            printf("Cannot find CPP file: %s\n",MOTOR_ProgramCppFile);
            ok = FALSE;
        }
    }

    if( !ok )
    {
        exit(0);
    }

    MOTOR_InitFlag = TRUE;
}

/******************************************************************************/

void MOTOR_Init( int argc, char *argv[], double loopfreq )
{
BYTE flag=MOTOR_FLAG_NONE;

    MOTOR_Init(argc,argv,flag,loopfreq);
}

/******************************************************************************/

void MOTOR_Init( int argc, char *argv[] )
{
BYTE flag=MOTOR_FLAG_NONE;
double loopfreq=0.0;

    MOTOR_Init(argc,argv,flag,loopfreq);
}

/******************************************************************************/

void MOTOR_Init( BYTE flag, double loopfreq )
{
    MOTOR_Init(0,NULL,(flag | MOTOR_FLAG_NOARGZERO),loopfreq);
}

/******************************************************************************/

void MOTOR_Init( double loopfreq )
{
BYTE flag=MOTOR_FLAG_NONE;

    MOTOR_Init(flag,loopfreq);
}

/******************************************************************************/

void MOTOR_Init( void )
{
BYTE flag=MOTOR_FLAG_NOARGZERO;
double loopfreq=0.0;

    MOTOR_Init(flag,loopfreq);
}

/******************************************************************************/

#ifndef MOTOR_MEX

BOOL MOTOR_Parameters( int argc, char *argv[], char *DataName, char *DataFile, char *TrialListFile, int &ConfigFileCount, STRING ConfigFileList[], BOOL (*ParameterFunction)( char code, char *data ) )
{
STRING ConfigFileParameters[CONFIG_FILES];
char code,*data;
BOOL ok;
int i;

    // Initialize MOTOR.LIB; Function exits if the initialization fails.
    MOTOR_Init(argc,argv);

    // Loop over each command-line parameter.
    for( ok=TRUE,i=1; ((i < argc) && ok); i++ )
    {
        // Process the parameter's code.
        code = CMDARG_code(argv[i],&data);
        switch( code )
        {
            case 'M' :
            case 'C' :
               ok = CMDARG_data(ConfigFileParameters,data,CONFIG_FILES);
               break;

            case 'D' :
               if( !CMDARG_data(DataName,data,STRLEN) )
               {
                   ok = FALSE;
               }
               else
               if( strstr(DataName,".") != NULL )
               {
                   ok = FALSE;
               }
               else
               if( STR_null(DataName) )
               {
                   ok = FALSE;
               }
               else
               {
                   strncpy(DataFile,STR_stringf("%s.DAT",DataName),STRLEN);
                   strncpy(TrialListFile,STR_stringf("%s-L.DAT",DataName),STRLEN);
               }
               break;

            default :
               ok = FALSE;

               if( ParameterFunction != NULL )
               {
                   ok = (*ParameterFunction)(code,data);
               }
               break;
        }

        if( !ok )
        {
            printf("Invalid parameter: %s\n",argv[i]);
        }
    }

    if( !ok )
    {
        return(FALSE);
    }

    // Process multiple config files or a meta-config file which specifies multiple files.
    ok = METACONFIG_Process(ConfigFileParameters,ConfigFileCount,ConfigFileList);

    if( !ok || (ConfigFileCount == 0) )
    {
        printf("Cannot process configuration file(s).\n");
        return(FALSE);
    }

    if( STR_null(DataName) )
    {
        printf("Data file not specified.\n");
        return(FALSE);
    }

    // Check disk space and file's existence.
    if( !DATAFILE_Check(DataFile) )
    {
        return(FALSE);
    }

    for( i=0; (i < ConfigFileCount); i++ )
    {
        printf("%02d %s\n",i,ConfigFileList[i]);
    }

    return(ok);
}

/******************************************************************************/

BOOL MOTOR_Parameters( int argc, char *argv[], char *DataName, char *DataFile, char *TrialListFile, int &ConfigFileCount, STRING ConfigFileList[] )
{
BOOL ok;

    ok = MOTOR_Parameters(argc,argv,DataName,DataFile,TrialListFile,ConfigFileCount,ConfigFileList,NULL);

    return(ok);
}

/******************************************************************************/

#endif


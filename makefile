INCLUDEPATH=u:\include\32bit
LIBPATH=u:\lib\32bit

#OBJLIST=version.obj matrix.obj mem.obj timer.obj isa.obj atift.obj general.obj socket.obj opto.obj optobuff.obj optodata.obj kalmanfilter.obj ekf.obj graphics.obj graphics_image.obj config.obj mmtimer.obj looptask.obj fob.obj str.obj spmx.obj goggles.obj trigger.obj cmdarg.obj com.obj kb.obj optorigid.obj atexit.obj file.obj stim.obj ni-daq.obj tracking.obj snms.obj dataproc.obj matdat.obj datafile.obj controller.obj sensoray.obj phantomisa.obj robot-object.obj robot.obj temptrak.obj mouse.obj cyberglove.obj beeper.obj waveplay.obj winfit.obj button.obj asensor.obj devices.obj ramper.obj permutelist.obj metaconfig.obj liberty.obj EyeLink1000.obj EyeT.obj pmove.obj s:\lib\win626.obj

OBJLIST=version.obj matrix.obj timer.obj kalmanfilter.obj ekf.obj spmx.obj config.obj mmtimer.obj looptask.obj str.obj cmdarg.obj kb.obj atexit.obj file.obj dataproc.obj controller.obj sensoray.obj phantomisa.obj robot-object.obj robot.obj temptrak.obj winfit.obj asensor.obj ramper.obj pmove.obj EyeLink1000.obj liberty.obj $(LIBPATH)\win626.obj

CC=cl /c /DAPI /DW32 /D_IS_MSDOS_ /DDOS /DOS_WIN32 /Iinclude /I$(INCLUDEPATH) /MT /O2

#motor.lib: include\motor.h include\typedefs.h $(OBJLIST)
#	lib /out:motor.lib $(OBJLIST) siappmt.lib
#	copy motor.lib $(LIBPATH)
#        copy include\motor.h $(INCLUDEPATH)
#	copy *.obj $(LIBPATH)

motor.lib: include\motor.h include\typedefs.h $(OBJLIST)
	lib /out:motor.lib $(OBJLIST) 
	copy motor.lib $(LIBPATH)
        copy include\motor.h $(INCLUDEPATH)
	copy *.obj $(LIBPATH)

$(INCLUDEPATH)\typedefs.h: include\typedefs.h
        copy include\typedefs.h $(INCLUDEPATH)

$(INCLUDEPATH)\motor.h: include\motor.h
        copy include\motor.h $(INCLUDEPATH)

version.obj: version.cpp include\version.h
        copy include\version.h $(INCLUDEPATH)
	$(CC) version.cpp

config.obj: config.cpp include\config.h
        copy include\config.h $(INCLUDEPATH)
	$(CC) config.cpp

mmtimer.obj: mmtimer.cpp include\mmtimer.h
        copy include\mmtimer.h $(INCLUDEPATH)
	$(CC) mmtimer.cpp

looptask.obj: looptask.cpp include\looptask.h
        copy include\looptask.h $(INCLUDEPATH)
	$(CC) looptask.cpp

kalmanfilter.obj: kalmanfilter.cpp include\kalmanfilter.h
        copy include\kalmanfilter.h $(INCLUDEPATH)
	$(CC) kalmanfilter.cpp

ekf.obj: ekf.cpp include\ekf.h
        copy include\ekf.h $(INCLUDEPATH)
	$(CC) ekf.cpp

matrix.obj: matrix.cpp mtx*.cpp include\matrix.h
        copy include\matrix.h $(INCLUDEPATH)
	$(CC) /DANSI matrix.cpp

robot-object.obj: robot*.cpp include\robot*.h include\controller.h include\kalmanfilter.h
        copy include\robot-object.h $(INCLUDEPATH)
	$(CC) robot-object.cpp

robot.obj: robot*.cpp include\robot*.h
        copy include\robot*.h $(INCLUDEPATH)
	$(CC) robot.cpp

timer.obj: timer.cpp include\version.h include\timer.h
        copy include\timer.h $(INCLUDEPATH)
	$(CC) timer.cpp

#isa.obj: isa.cpp include\version.h include\isa.h
#        copy include\isa.h $(INCLUDEPATH)
#	$(CC) isa.cpp

#atift.obj: atift.cpp include\version.h include\isa.h include\atift.h
#        copy include\atift.h $(INCLUDEPATH)
#	$(CC) atift.cpp

#mem.obj: mem.cpp include\version.h include\mem.h
#        copy include\mem.h $(INCLUDEPATH)
#	$(CC) mem.cpp

#general.obj: general.cpp include\general.h
#        copy include\general.h $(INCLUDEPATH)
#	$(CC) /DDOS general.cpp

#graphics.obj: graphics.cpp graphics-old.cpp include\graphics.h include\graphics-old.h
#        copy include\graphics.h $(INCLUDEPATH)
#        copy include\graphics-old.h $(INCLUDEPATH)
#	$(CC) /DDOS graphics.cpp

#graphics_image.obj: graphics_image.cpp include\graphics_image.h
#        copy include\graphics_image.h $(INCLUDEPATH)
#        copy include\glext2.h $(INCLUDEPATH)
#	$(CC) /DDOS graphics_image.cpp

#net.obj: net.cpp include\net.h
#        copy include\net.h $(INCLUDEPATH)
#	$(CC) net.cpp

#opto.obj: opto.cpp opto*.cpp include\version.h include\opto*.h
#        copy include\opto*.h $(INCLUDEPATH)
#	$(CC) opto.cpp

#optobuff.obj: optobuff.cpp opto*.cpp include\version.h include\opto*.h
#        copy include\opto*.h $(INCLUDEPATH)
#	$(CC) optobuff.cpp

#optodata.obj: optodata.cpp opto*.cpp include\version.h include\opto*.h
#        copy include\opto*.h $(INCLUDEPATH)
#	$(CC) optodata.cpp

#optorigid.obj: optorigid.cpp opto*.cpp include\version.h include\opto*.h include\optorigid.h
#        copy include\optorigid.h $(INCLUDEPATH)
#	$(CC) optorigid.cpp
 
#fob.obj: fob.cpp fob*.cpp include\fob.h
#        copy include\fob.h $(INCLUDEPATH)
#	$(CC) fob.cpp

str.obj: str.cpp include\str.h
        copy include\str.h $(INCLUDEPATH)
	$(CC) str.cpp

#com.obj: com.cpp include\com.h
#        copy include\com.h $(INCLUDEPATH)
#	$(CC) com.cpp

spmx.obj: spmx.cpp include\spmx.h
        copy include\spmx.h $(INCLUDEPATH)
	$(CC) spmx.cpp

#goggles.obj: goggles.cpp include\goggles.h
#        copy include\goggles.h $(INCLUDEPATH)
#	$(CC) goggles.cpp

#trigger.obj: trigger.cpp sequencer.cpp include\trigger.h include\sequencer.h
#        copy include\trigger.h $(INCLUDEPATH)
#        copy include\sequencer.h $(INCLUDEPATH)
#	$(CC) trigger.cpp

cmdarg.obj: cmdarg.cpp include\cmdarg.h
        copy include\cmdarg.h $(INCLUDEPATH)
	$(CC) cmdarg.cpp

kb.obj: kb.cpp include\kb.h
        copy include\kb.h $(INCLUDEPATH)
	$(CC) kb.cpp

atexit.obj: atexit.cpp include\atexit.h
        copy include\atexit.h $(INCLUDEPATH)
	$(CC) atexit.cpp

file.obj: file.cpp include\file.h
        copy include\file.h $(INCLUDEPATH)
	$(CC) file.cpp

#stim.obj: stim.cpp include\stim.h
#        copy include\stim.h $(INCLUDEPATH)
#	$(CC) stim.cpp

#snms.obj: snms.cpp include\snms.h
#        copy include\snms.h $(INCLUDEPATH)
#	$(CC) snms.cpp

dataproc.obj: dataproc.cpp include\dataproc.h
        copy include\dataproc.h $(INCLUDEPATH)
	$(CC) dataproc.cpp

#datafile.obj: datafile.cpp include\datafile.h
#        copy include\datafile.h $(INCLUDEPATH)
#	$(CC) datafile.cpp

#matdat.obj: matdat.cpp include\matdat.h
#        copy include\matdat.h $(INCLUDEPATH)
#	$(CC) matdat.cpp

#ni-daq.obj: ni-daq.cpp include\ni-daq.h
#        copy include\ni-daq.h $(INCLUDEPATH)
#	$(CC) ni-daq.cpp

controller.obj: controller.cpp include\controller.h
        copy include\controller.h $(INCLUDEPATH)
	$(CC) controller.cpp

sensoray.obj: sensoray.cpp include\sensoray.h
        copy include\sensoray.h $(INCLUDEPATH)
	$(CC) sensoray.cpp

phantomisa.obj: phantomisa.cpp include\phantomisa.h
        copy include\phantomisa.h $(INCLUDEPATH)
	$(CC) phantomisa.cpp

temptrak.obj: temptrak.cpp include\temptrak.h
        copy include\temptrak.h $(INCLUDEPATH)
	$(CC) temptrak.cpp

#mouse.obj: mouse.cpp include\mouse.h
#        copy include\mouse.h $(INCLUDEPATH)
#	$(CC) mouse.cpp

#tracking.obj: tracking.cpp opto.cpp fob*.cpp phantom.cpp mouse.cpp include\tracking.h include\opto.h include\fob.h include\phantom.h include\mouse.h
#        copy include\tracking.h $(INCLUDEPATH)
#	$(CC) tracking.cpp

#beeper.obj: beeper.cpp include\beeper.h
#        copy include\beeper.h $(INCLUDEPATH)
#	$(CC) beeper.cpp

#waveplay.obj: waveplay.cpp include\waveplay.h
#        copy include\waveplay.h $(INCLUDEPATH)
#	$(CC) waveplay.cpp

winfit.obj: winfit.cpp include\winfit.h
        copy include\winfit.h $(INCLUDEPATH)
	$(CC) winfit.cpp

ramper.obj: ramper.cpp include\ramper.h
        copy include\ramper.h $(INCLUDEPATH)
	$(CC) ramper.cpp

#permutelist.obj: permutelist.cpp include\permutelist.h
#        copy include\permutelist.h $(INCLUDEPATH)
#	$(CC) permutelist.cpp

#metaconfig.obj: metaconfig.cpp include\metaconfig.h
#        copy include\metaconfig.h $(INCLUDEPATH)
#	$(CC) metaconfig.cpp

liberty.obj: liberty.cpp include\liberty.h
        copy include\liberty.h $(INCLUDEPATH)
	$(CC) liberty.cpp

#cyberglove.obj: cyberglove.cpp include\cyberglove.h
#        copy include\cyberglove.h $(INCLUDEPATH)
#	$(CC) cyberglove.cpp

EyeLink1000.obj: EyeLink1000.cpp include\EyeLink1000.h
        copy include\EyeLink1000.h $(INCLUDEPATH)
	$(CC) EyeLink1000.cpp

#EyeT.obj: EyeT.cpp include\EyeT.h
#        copy include\EyeT.h $(INCLUDEPATH)
#	$(CC) EyeT.cpp

#socket.obj: socket.cpp include\socket.h
#        copy include\socket.h $(INCLUDEPATH)
#	$(CC) /DCRTAPI1=_cdecl /DCRTAPI2=_cdecl /D_X86_=1 /D_WINNT /D_WIN32_WINNT=0x0400 /DWINVER=0x0400 /DWIN32 /D_WIN32 socket.cpp

pmove.obj: pmove.cpp include\pmove.h
        copy include\pmove.h $(INCLUDEPATH)
	$(CC) pmove.cpp

#button.obj: button.cpp include\button.h
#asensor.obj: asensor.cpp include\asensor.h
#devices.obj: devices.cpp include\devices.h

.cpp.obj:
        copy include\$*.h $(INCLUDEPATH)
	$(CC) $*.cpp
	copy $*.obj $(LIBPATH)


/******************************************************************************/
/*                                                                            */
/* MODULE  : GRAPHICS_Image.cpp                                               */
/*                                                                            */
/* PURPOSE : GRAPHICS sub-module to display BMP images.                       */
/*                                                                            */
/* DATE    : 22/Oct/2016                                                      */
/*                                                                            */
/* CHANGES                                                                    */
/*                                                                            */
/* V1.0  JNI 22/Oct/2016 - Initial development from local application code.   */
/*                                                                            */
/******************************************************************************/

#include <motor.h>

/******************************************************************************/
/******************************************************************************/

AUX_RGBImageRec2 *auxDIBImageLoad2( const char *FileName )
{ 
    return new AUX_RGBImageRec2(FileName);
}

/*****************************************************************************/

void AUX_RGBImageRec2::convertBGRtoRGB( void )
{
const DWORD BitmapLength = sizeX * sizeY * 4;

byte Temp;  

    // Swap blue and red bytes in quadruplets.
    for( DWORD i=0; (i < BitmapLength); i+=4 ) 
    {
        Temp = data[i];
    	data[i] = data[i+2];
    	data[i+2] = Temp;
    }
}

/*****************************************************************************/

AUX_RGBImageRec2::AUX_RGBImageRec2( const char *FileName ): data(NULL), NoErrors(false)
{ 
bool ok;

    ok = loadFile(FileName);
}

/*****************************************************************************/

AUX_RGBImageRec2::~AUX_RGBImageRec2( void )
{
    if( data != NULL )
    {
        delete data;
        data = NULL;
    }   
}

/*****************************************************************************/

bool AUX_RGBImageRec2::loadFile(const char* Filename)
{
BITMAPINFO BMInfo;		     // need the current OpenGL device contexts in order to make use of windows DIB utilities  
const HDC gldc = wglGetCurrentDC();  // a handle for the current OpenGL Device Contexts
HANDLE DIBHandle=NULL;
int rc;
		
    // Release old data since this object could be used to load multiple Textures  
    NoErrors = false;  								
    if( data != NULL )
    {
        delete data;					// windows needs this info to determine what header info we are looking for  
    }

    BMInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);  // Get windows to determine color bit depth in the file for us  
    BMInfo.bmiHeader.biBitCount = 0;				// Get windows to open and load the BMP file and handle the messy decompression if the file is compressed  

    // Use windows to get header info of bitmap - assume no errors in header format.
    DIBHandle = LoadImage(NULL,Filename,IMAGE_BITMAP,0,0,LR_DEFAULTCOLOR | LR_CREATEDIBSECTION | LR_LOADFROMFILE);  

    if( DIBHandle == NULL )
    {
        return(false);
    }

    rc = GetDIBits(gldc,(HBITMAP)DIBHandle,0,0,NULL,&BMInfo,DIB_RGB_COLORS);

    if( rc == 0 )
    {
        return(false);
    }

    sizeX = BMInfo.bmiHeader.biWidth;
    sizeY = BMInfo.bmiHeader.biHeight;                     // change color depth to 24 bits (3 bytes (BGR) / pixel)  
    BMInfo.bmiHeader.biBitCount = 32;                      // don't want the data compressed  
    BMInfo.bmiHeader.biCompression = BI_RGB;  
    const DWORD BitmapLength = sizeX * sizeY * 4;	   // 3 bytes (BGR) per pixel (24bp)  

    // allocate enough memory to hold the pixel data in client memory  
    data = new byte[BitmapLength];

    // Get windows to do the dirty work of converting the BMP into the format needed by OpenGL  
    // if file is already 24 bit color then this is a waste of time but makes for short code  
    // Get the actual Texel data from the BMP object  
	
    if( GetDIBits(gldc,(HBITMAP)DIBHandle,0,sizeY,data,&BMInfo,DIB_RGB_COLORS) ) 
    {
        NoErrors = true;

        // NOTE: BMP is in BGR format but OpenGL needs RGB unless you use GL_BGR_EXT
        convertBGRtoRGB();							
     }

    // don't need the BMP Object anymore  

    DeleteObject(DIBHandle);						

    return(NoErrors);
}   

/*****************************************************************************/

AUX_RGBImageRec2 *ImageLoadBMP(char *Filename)                // Loads A Bitmap Image
{
FILE *File;

    if( Filename != NULL )
    {
        // Quick check To See If The File Exists
        if( (File=fopen(Filename,"r")) != NULL )                      
        {
            fclose(File);                           // Close The Handle
            return(auxDIBImageLoad2(Filename));     // Load The Bitmap And Return A Pointer
        }
    }

    // If Load Failed Return NULL.
    return(NULL);
}

/******************************************************************************/
/******************************************************************************/

BOOL    GRAPHICS_ImageStartFlag=FALSE;

GLuint  GRAPHICS_ImageTextureID[GRAPHICS_IMAGE_MAX];
BOOL    GRAPHICS_ImageTextureUsedFlag[GRAPHICS_IMAGE_MAX];
GRAPHICS_Image *GRAPHICS_ImageList[GRAPHICS_IMAGE_MAX];

/******************************************************************************/

void GRAPHICS_ImageStart( void )
{
int i;

    if( GRAPHICS_ImageStartFlag )
    {
        return;
    }

    // Create the OpenGL textures.
    glGenTextures(GRAPHICS_IMAGE_MAX,GRAPHICS_ImageTextureID);

    for( i=0; (i < GRAPHICS_IMAGE_MAX); i++ )
    {
        GRAPHICS_ImageTextureUsedFlag[i] = FALSE;
        GRAPHICS_ImageList[i] = NULL;
        printf("GRAPHICS_ImageInit() TextureID[%d]=%d\n",i,GRAPHICS_ImageTextureID[i]);
    }

    GRAPHICS_ImageStartFlag = TRUE;
}

/******************************************************************************/

void GRAPHICS_ImageStop( void )
{
int i;

    if( !GRAPHICS_ImageStartFlag )
    {
        return;
    }

    for( i=0; (i < GRAPHICS_IMAGE_MAX); i++ )
    {
        if( GRAPHICS_ImageTextureUsedFlag[i] && (GRAPHICS_ImageList[i] != NULL) )
        {
            GRAPHICS_ImageList[i]->Close();
        }
    }
}

/******************************************************************************/

int GRAPHICS_ImageTextureFree( void )
{
int index;

    // Find an OpenGL texture that isn't in use.
    for( index=0; ((index < GRAPHICS_IMAGE_MAX) && GRAPHICS_ImageTextureUsedFlag[index]); index++ );

    if( index >= GRAPHICS_IMAGE_MAX )
    {
        index = -1;
    }

    return(index);
}

/******************************************************************************/

GRAPHICS_Image::GRAPHICS_Image( void )
{
    OpenFlag = FALSE;
    LoadedFlag = FALSE;
    memset(FileName,0,STRLEN);
    TextureImage = NULL;
    AspectRatio = 0.0;
    DrawLatency = new TIMER_Interval;
}

/******************************************************************************/

GRAPHICS_Image::~GRAPHICS_Image( void )
{
    Close();
    delete DrawLatency;
}

/******************************************************************************/

BOOL GRAPHICS_Image::Open( void )
{
STRING filename="";
double xsize=0.0,ysize=0.0;
BOOL ok;

    ok = Open(filename,xsize,ysize);

    return(ok);
}

/******************************************************************************/

BOOL GRAPHICS_Image::Open( char *filename, double xsize, double ysize )
{
    if( !(GRAPHICS_Initialized && GRAPHICS_Started) )
    {
        GRAPHICS_errorf("GRAPHICS_ImageOpen(filename=%s,...) GRAPHICS module not initialized.\n",filename);
        return(FALSE);
    }

    GRAPHICS_ImageStart();

    if( OpenFlag )
    {
        GRAPHICS_errorf("GRAPHICS_ImageOpen(filename=%s,...) Already open.\n",filename);
        return(FALSE);
    }

    if( (TextureIndex=GRAPHICS_ImageTextureFree()) == -1 )
    {
        GRAPHICS_errorf("GRAPHICS_ImageOpen(filename=%s,...) No free textures.\n",filename);
        return(FALSE);
    }

    TextureID = GRAPHICS_ImageTextureID[TextureIndex];
    GRAPHICS_ImageList[TextureIndex] = this;
    GRAPHICS_messgf("GRAPHICS_ImageOpen(filename=%s,xsize=%.2lf,ysize=%.2fl)\n",filename,xsize,ysize);
    GRAPHICS_messgf("TextureIndex=%d, TextureID=%d\n",TextureIndex,TextureID);

    glBindTexture(GL_TEXTURE_2D,TextureID);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);

    if( !STR_null(filename) )
    {
        if( !Load(filename,xsize,ysize) )
        {
            return(FALSE);
        }
    }

    GRAPHICS_ImageTextureUsedFlag[TextureIndex] = TRUE;

    OpenFlag = TRUE;

    return(TRUE);
}

/******************************************************************************/

BOOL GRAPHICS_Image::Open( char *filename, double xsize )
{
BOOL ok;
double ysize=0.0;

    ok = Open(filename,xsize,ysize);

    return(ok);
    
}

/******************************************************************************/

void GRAPHICS_Image::Close( void )
{
    if( OpenFlag && (DrawLatency->Count() > 0) )
    {
        DrawLatency->Results();
    }

    OpenFlag = FALSE;
    LoadedFlag = FALSE;
    GRAPHICS_ImageTextureUsedFlag[TextureIndex] = FALSE;
    GRAPHICS_ImageList[TextureIndex] = NULL;
}

/******************************************************************************/

BOOL GRAPHICS_Image::Load( char *filename, double xsize, double ysize )
{
BOOL ok=TRUE;

    if( !OpenFlag )
    {
        ok = Open();
    }

    if( !ok )
    {
        GRAPHICS_errorf("GRAPHICS_ImageLoad(filename=%s,...) Cannot initialize.\n",filename);
    }

    if( DrawLatency->Count() > 0 )
    {
        DrawLatency->Results();
    }

    if( (TextureImage=ImageLoadBMP(filename)) == NULL )
    {
        GRAPHICS_errorf("GRAPHICS_ImageLoad(filename=%s,...) Cannot load BMP file.\n",filename);
        return(FALSE);
    }

    GRAPHICS_messgf("GRAPHICS_ImageLoad(filename=%s,xsize=%.2lf,ysize=%.2lf)\n",filename,xsize,ysize);
    GRAPHICS_messgf("xpixels=%ld, ypixels=%ld\n",TextureImage->sizeX,TextureImage->sizeY);

    glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,TextureImage->sizeX,TextureImage->sizeY,0,GL_RGBA,GL_UNSIGNED_BYTE,TextureImage->data);

    Xpixels = TextureImage->sizeX;
    Ypixels = TextureImage->sizeY;

    AspectRatio = (double)Xpixels / (double)Ypixels;

    free(TextureImage->data);
    free(TextureImage);

    strncpy(FileName,filename,STRLEN);

    Xsize = xsize;
    Xhalf = xsize/2.0;

    if( ysize == 0.0 )
    {
        ysize = xsize / AspectRatio;
    }

    Ysize = ysize;
    Yhalf = ysize/2.0;

    DrawLatency->Reset(filename);

    LoadedFlag = TRUE;

    return(TRUE);
}

/******************************************************************************/

BOOL GRAPHICS_Image::Load( char *filename, double xsize )
{
BOOL ok;
double ysize=0.0;

    ok = Load(filename,xsize,ysize);

    return(ok);
    
}

/******************************************************************************/

void GRAPHICS_Image::Draw( matrix *posn )
{
double scale=1.0,iscale;

    if( !(OpenFlag && LoadedFlag) )
    {
        return;
    }

    DrawLatency->Before();

    GRAPHICS_ColorSet(WHITE);

    glEnable(GL_TEXTURE_2D);
    glPushMatrix();

    if( posn != NULL )
    {
        glTranslatef((*posn)(1,1),(*posn)(2,1),(*posn)(3,1));
    }

    glBindTexture(GL_TEXTURE_2D,TextureID);

    iscale = 1.0 / scale;

    glBegin(GL_QUADS);
    glTexCoord2f((1.0-iscale)/2.0,(1.0-iscale)/2.0); glVertex3f(-Xhalf,-Yhalf,0.0f); // Bottom Left
    glTexCoord2f((1.0+iscale)/2.0,(1.0-iscale)/2.0); glVertex3f( Xhalf,-Yhalf,0.0f); // Bottom Right
    glTexCoord2f((1.0+iscale)/2.0,(1.0+iscale)/2.0); glVertex3f( Xhalf, Yhalf,0.0f); // Top Right
    glTexCoord2f((1.0-iscale)/2.0,(1.0+iscale)/2.0); glVertex3f(-Xhalf, Yhalf,0.0f); // Top Left
    glEnd();
												
    glDisable(GL_TEXTURE_2D);
    glPopMatrix();

    DrawLatency->After();
}

/******************************************************************************/

void GRAPHICS_Image::Draw( void )
{
    Draw(NULL);
}

/******************************************************************************/

void GRAPHICS_Image::ImagePixels( int &xpixels, int &ypixels )
{
    if( OpenFlag && LoadedFlag )
    {
        xpixels = Xpixels;
        ypixels = Ypixels;
    }
}

/******************************************************************************/

void GRAPHICS_Image::ImageSize( int &xsize, int &ysize )
{
    if( OpenFlag && LoadedFlag )
    {
        xsize = Xsize;
        ysize = Ysize;
    }
}

/******************************************************************************/


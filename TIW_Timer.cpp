#include "TIW_Timer.h"

CRITICAL_SECTION CriticalSection;
TIMER ACPITImer("LoopTask");


void ErrorHandler(char * lpszFunction) { 
    // TODO: catch/alert error safely rather than ignore
}


DWORD WINAPI CheckTime( void * lpParam){
    /**
     * Syncronised helper function to check ellapsed seconds from timer
     *
     * @param lpParam: void pointer THRD_DATA struct with timer data
     *
    **/

    EnterCriticalSection(&CriticalSection); 
    // For the duration of the test
    while (((THRD_DATA *)lpParam )->sigma_current <  ((THRD_DATA *)lpParam )->duration ){
        Sleep(2);
        ((THRD_DATA *)lpParam )->sigma_current = ACPITImer.ElapsedSeconds();

        while( ((THRD_DATA *)lpParam )->sigma_current - ((THRD_DATA *)lpParam )->sigma_previous < ((THRD_DATA *)lpParam )->frequency){
            ((THRD_DATA *)lpParam )->sigma_current = ACPITImer.ElapsedSeconds();
        }

        ((THRD_DATA *)lpParam )->sigma_previous = ((THRD_DATA *)lpParam )->sigma_current;

        ((THRD_DATA *)lpParam )->callback(); // callback
        // Signal for B
        ((THRD_DATA *)lpParam )->a_on = 1;
    }

    LeaveCriticalSection(&CriticalSection);

    return 0; 
} 


void TIWTimer( int duration, int frequency,  void (*callback)(void)  ){
    /**
     * Multithreaded timer implmentation in order to allow real time timer
     * callbacks on windows, required for accurate motor control
     *
     * @param duration: timer duration
     * @param frequency: frequency of callback
     * @param callback: void function pointer for callback
     *
     * Timer implemented as per:  https://github.com/franciscovargas/CBLLabNotes/blob/master/sofware_timer.pdf
    **/


    DWORD   dwThreadIdArray[THREAD_NUM];
    HANDLE  hThreadArray[THREAD_NUM]; 
    // Crit init
    if (!InitializeCriticalSectionAndSpinCount(&CriticalSection, 0x00000400) ) 
        return ;

    THRD_DATA * shrd_sigma = (THRD_DATA *) HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,
            sizeof(THRD_DATA));
    
    if( shrd_sigma == NULL )
        ExitProcess(2);

    shrd_sigma->duration = duration; 
    shrd_sigma->frequency = frequency;
    shrd_sigma->callback = callback;
    
    ACPITImer.Reset();
    shrd_sigma->sigma_previous = ACPITImer.ElapsedSeconds();

    // Starting the 2 worker threads in suspended mode
    for( int i=0; i < THREAD_NUM; i++ ){

        hThreadArray[i] = CreateThread( 
            NULL,                  
            0,                       
            CheckTime,       
            shrd_sigma,   // shared resource
            0x00000004,                
            &dwThreadIdArray[i] 
        ); 

        if (hThreadArray[i] == NULL) {
           ErrorHandler(TEXT("CreateThread"));
           ExitProcess(3);
        }
    }


    DWORD WINAPI SuspendCountA = ResumeThread(hThreadArray[0]);
    while(!shrd_sigma->a_on); // Waiting on thread A to start thread B
    DWORD WINAPI SuspendCountB =  ResumeThread(hThreadArray[1]);


    WaitForMultipleObjects(THREAD_NUM, hThreadArray, TRUE, INFINITE);

    // Closing thread handles
    for(int i=0; i<THREAD_NUM; i++){
        CloseHandle(hThreadArray[i]);

    }

    if(shrd_sigma!= NULL){
        HeapFree(GetProcessHeap(), 0, shrd_sigma);
        shrd_sigma = NULL;    // Ensure address is not reused.
    }
    DeleteCriticalSection(&CriticalSection);

    
}